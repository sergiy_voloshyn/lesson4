package com.company;

public final class Generator {

    private Generator() {
    }

    public static Book[] generateBooks() {
        Book[] books = new Book[3];
        books[0] = new Book("Tolkien", 1920);
        books[1] = new Book("Tolstoy", 1820);
        books[2] = new Book("Pushkin", 1720);
        return books;
    }
}