package com.company;

import com.sun.org.apache.xpath.internal.operations.Equals;

/**
 * Created by Student on 28.11.2017.
 */


public class Book {
    String author;
    int year;

    public Book(String author, int year) {
        this.author = author;
        this.year = year;
    }

    public Book() {

    }

    public boolean isOlderThen(Book book) {

        return year < book.year;
    }

    public String toString() {

        return String.format("Book: %s: %d", author, year);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Book book = (Book) o;

        if (year != book.year) return false;
        return author != null ? author.equals(book.author) : book.author == null;
    }


}


