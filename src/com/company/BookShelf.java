package com.company;

/**
 * Created by Student on 28.11.2017.
 */
public class BookShelf {
    Book[] books;

    public BookShelf(Book[] books) {
        this.books = books;
    }


    public Book getOldestBook() {

        Book book = books[0];

        for (int i = 0; i < books.length; i++) {

            if (books[i].isOlderThen(book)) {

                book = books[i];
            }
        }
        return book;
    }

    public boolean hasAuthor(String authorName) {


        for (int i = 0; i < books.length; i++) {
            if ((books[i].author.toLowerCase().contains(authorName.toLowerCase()))) {
                return true;
            }
        }
        return false;
    }

    public boolean isOldestBook(int year) {


        for (int i = 0; i < books.length; i++) {
            if (books[i].year < year) {
                return true;
            }
        }
        return false;
    }


    public void printAllBook() {

        for (int i = 0; i < books.length; i++) {
            System.out.println(String.format("Книга %d: %s: %d", i, books[i].author, books[i].year));

        }
    }

    public int getSizeShelf(){

        return books.length;
    }



}
