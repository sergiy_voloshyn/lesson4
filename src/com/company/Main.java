package com.company;

import static com.company.Generator.generateBooks;

public class Main {

    public static void main(String[] args) {
        BookShelf bookShelf = new BookShelf(generateBooks());

        bookShelf.printAllBook();

        Book oldBook = bookShelf.getOldestBook();
        System.out.println(oldBook.author);

        if (bookShelf.hasAuthor("Pushkin")) {
            System.out.println("Pushkin is present.");
        } else {
            System.out.println("Pushkin isn't present.");
        }


        if (bookShelf.isOldestBook(1900)) {
            System.out.println("Book is present.");
        } else {
            System.out.println("Book isn't present.");
        }

        for (int i = 0; i < bookShelf.getSizeShelf(); i++) {
            System.out.println(bookShelf.books[i].toString());
        }

        Book book1 = new Book("Tolstoy", 1820);
        for (int i = 0; i < bookShelf.getSizeShelf(); i++) {
            if (bookShelf.books[i].equals(book1))
                System.out.println("Books the same! " + book1.toString());

        }

    }
}